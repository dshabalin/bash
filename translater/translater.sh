#!/usr/bin/env bash
sel_text=$(xsel -o | sed "s/[\"'<>]//g")

if [[ ${#sel_text} -lt 2 ]] 
then
echo err
exit 1
fi

result_raw=$(curl -X GET "https://translate.yandex.net/api/v1.5/tr.json/translate?key=trnsl.1.1.20160330T215754Z.d92802209a94f85a.d93fa6ce17c314263cc462668ddd516d42e1c8f5&text=$sel_text&lang=en-ru&format=plain")

#TODO: add normal regexp to get "text" from api response
#	{"code":200,"lang":"en-ru","text":["purpose"]}
result=$(echo $result_raw | grep -Po '[А-я]+')

notify-send -u critical "$sel_text" "$result"

exit 0
